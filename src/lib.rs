pub mod day1;
pub mod day2;
pub mod day3;
pub mod day4;
pub mod day5;

pub fn load_input_trim_lines(input: &str) -> Vec<String> {
    input
        .split('\n')
        .filter_map(|l| {
            let s = l.trim();
            if s.is_empty() {
                None
            } else {
                Some(s)
            }
        })
        .map(String::from)
        .collect()
}
