use std::{collections::HashMap, fs};

pub fn run_me() -> u32 {
    let input = fs::read_to_string("src/day3/input").expect("could not read input file");
    find_part_nums(crate::load_input_trim_lines(&input))
}

pub fn run_me_next() -> u32 {
    let input = fs::read_to_string("src/day3/input").expect("could not read input file");
    find_gear_ratios(crate::load_input_trim_lines(&input))
}

fn find_part_nums(lines: Vec<String>) -> u32 {
    let map = lines_to_chars(lines);
    let mut part_nums: Vec<u32> = vec![];
    for (i, line) in map.iter().enumerate() {
        let mut col_iter = line.iter().enumerate();
        let mut cur_pair = col_iter.next();
        while cur_pair.is_some() {
            let (mut j, mut ch) = cur_pair.unwrap();
            if ch.is_ascii_digit() {
                let mut is_pn = false;
                let mut n_chars: Vec<char> = vec![];
                while ch.is_ascii_digit() && cur_pair.is_some() {
                    n_chars.push(*ch);
                    is_pn = if !is_pn {
                        check_neighbors(i, j, &map)
                    } else {
                        is_pn
                    };
                    cur_pair = col_iter.next();
                    if let Some(p) = cur_pair {
                        (j, ch) = p;
                    } else {
                        break;
                    }
                }
                if is_pn {
                    part_nums.push(n_chars.into_iter().collect::<String>().parse().unwrap());
                }
            }
            cur_pair = col_iter.next();
        }
    }
    // println!("{:?}", part_nums);
    part_nums.iter().sum()
}

fn is_symbol(ch: &char) -> bool {
    !ch.is_ascii_digit() && *ch != '.'
}

fn symbol_at_index(i: usize, j: usize, map: &[Vec<char>]) -> bool {
    match map.get(i) {
        Some(row) => match row.get(j) {
            Some(c) => is_symbol(c),
            None => false,
        },
        None => false,
    }
}

fn check_neighbors(i: usize, j: usize, map: &[Vec<char>]) -> bool {
    let down_ok = j.checked_sub(1).is_some();
    let left_ok = i.checked_sub(1).is_some();
    symbol_at_index(i, j+1, map) // above
        || symbol_at_index(i+1, j+1, map) // above-right
        || symbol_at_index(i+1, j, map) // right
        || (down_ok && symbol_at_index(i+1, j-1, map)) // below-right
        || (down_ok && symbol_at_index(i, j-1, map)) // below
        || (left_ok && down_ok && symbol_at_index(i-1, j-1, map)) // below-left
        || (left_ok && symbol_at_index(i-1, j, map)) // left
        || (left_ok && symbol_at_index(i-1, j+1, map)) // above-left
}

fn lines_to_chars(lines: Vec<String>) -> Vec<Vec<char>> {
    let mut rows: Vec<Vec<char>> = vec![];
    for line in lines {
        let mut cols: Vec<char> = vec![];
        for ch in line.chars() {
            cols.push(ch);
        }
        rows.push(cols);
    }
    rows
}

fn find_gear_ratios(lines: Vec<String>) -> u32 {
    let map = lines_to_chars(lines);
    let mut part_nums: Vec<(u32, (usize, usize))> = vec![];
    for (i, line) in map.iter().enumerate() {
        let mut col_iter = line.iter().enumerate();
        let mut cur_pair = col_iter.next();
        while cur_pair.is_some() {
            let (mut j, mut ch) = cur_pair.unwrap();
            if ch.is_ascii_digit() {
                let mut n_chars: Vec<char> = vec![];
                let mut gear: Option<(usize, usize)> = None;
                while ch.is_ascii_digit() && cur_pair.is_some() {
                    n_chars.push(*ch);
                    gear = gear.or(find_gears(i, j, &map));
                    cur_pair = col_iter.next();
                    if let Some(p) = cur_pair {
                        (j, ch) = p;
                    } else {
                        break;
                    }
                }
                if let Some(g) = gear {
                    part_nums.push((n_chars.into_iter().collect::<String>().parse().unwrap(), g));
                }
            }
            cur_pair = col_iter.next();
        }
    }
    // println!("{:?}", part_nums);
    calc_sum_gear_ratios(part_nums)
}

fn calc_sum_gear_ratios(part_nums: Vec<(u32, (usize, usize))>) -> u32 {
    let mut gear_map: HashMap<(usize, usize), Vec<u32>> = HashMap::new();
    for pair in part_nums {
        gear_map
            .entry(pair.1)
            .and_modify(|v| v.push(pair.0))
            .or_insert(vec![pair.0]);
    }
    let mut sum: u32 = 0;
    for (_, val) in gear_map {
        match val.len() {
            2 => sum += val[0] * val[1],
            3.. => println!("multiple gear ratio! {:?}", val),
            _ => (),
        }
    }
    sum
}

fn is_gear(ch: &char) -> bool {
    *ch == '*'
}

fn gear_at_index(i: usize, j: usize, map: &[Vec<char>]) -> Option<(usize, usize)> {
    match map.get(i) {
        Some(row) => match row.get(j) {
            Some(c) => {
                if is_gear(c) {
                    Some((i, j))
                } else {
                    None
                }
            }
            None => None,
        },
        None => None,
    }
}

fn find_gears(i: usize, j: usize, map: &[Vec<char>]) -> Option<(usize, usize)> {
    let down = j.checked_sub(1);
    let left = i.checked_sub(1);
    gear_at_index(i, j + 1, map) // above
        .or(gear_at_index(i + 1, j + 1, map)) // above-right
        .or(gear_at_index(i + 1, j, map)) // right
        .or(down.and_then(|_| gear_at_index(i + 1, j - 1, map))) // below-right
        .or(down.and_then(|_| gear_at_index(i, j - 1, map))) // below
        .or(left
            .and(down)
            .and_then(|_| gear_at_index(i - 1, j - 1, map))) // below-left
        .or(left.and_then(|_| gear_at_index(i - 1, j, map))) // left
        .or(left.and_then(|_| gear_at_index(i - 1, j + 1, map))) // above-left
}

#[cfg(test)]
mod tests {
    use crate::load_input_trim_lines;

    const INPUT: &str = "467..114..
    ...*......
    ..35..633.
    ......#...
    617*......
    .....+.58.
    ..592.....
    ......755.
    ...$.*....
    .664.598..";

    #[test]
    fn pt1() {
        assert_eq!(
            crate::day3::find_part_nums(load_input_trim_lines(INPUT)),
            4361
        );
    }

    #[test]
    fn pt2() {
        let map = crate::day3::lines_to_chars(load_input_trim_lines(INPUT));
        println!(">>> {:?}", crate::day3::find_gears(7, 6, &map));
        assert_eq!(
            crate::day3::find_gear_ratios(load_input_trim_lines(INPUT)),
            467835
        );
    }
}
