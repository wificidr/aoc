use std::collections::{HashMap, HashSet};
use std::fs;

pub fn run_me() -> u32 {
    let input = fs::read_to_string("src/day4/input").expect("could not read input file");
    process_games(lines_to_tickets(crate::load_input_trim_lines(&input)))
}

pub fn run_me_next() -> u32 {
    let input = fs::read_to_string("src/day4/input").expect("could not read input file");
    process_games_pt2(lines_to_tickets(crate::load_input_trim_lines(&input)))
}

fn process_games(games: Vec<(HashSet<u32>, HashSet<u32>)>) -> u32 {
    let base: u32 = 2;
    games
        .iter()
        .map(|pair| pair.0.intersection(&pair.1))
        .filter_map(|s| {
            if s.clone().count() == 0 {
                None
            } else {
                Some(s.count())
            }
        })
        .map(|c| base.pow(u32::try_from(c - 1).unwrap()))
        .sum()
}

fn process_games_pt2(games: Vec<(HashSet<u32>, HashSet<u32>)>) -> u32 {
    let mut cards: HashMap<usize, usize> = HashMap::new();
    for (i, _) in games.iter().enumerate() {
        cards.insert(i + 1, 1);
    }
    for (i, pair) in games.iter().enumerate() {
        let c = pair.0.intersection(&pair.1).count();
        let cur_count = *cards.get(&(i + 1)).unwrap();
        for j in i + 2..c + i + 2 {
            cards.entry(j).and_modify(|v| *v += cur_count);
        }
    }
    u32::try_from(cards.values().sum::<usize>()).unwrap()
}

fn lines_to_tickets(lines: Vec<String>) -> Vec<(HashSet<u32>, HashSet<u32>)> {
    let mut games: Vec<(HashSet<u32>, HashSet<u32>)> = vec![];
    for line in lines {
        let parts = line.split(':').collect::<Vec<&str>>()[1]
            .split('|')
            .collect::<Vec<&str>>();
        games.push((part_to_nums(parts[0]), part_to_nums(parts[1])));
    }
    games
}

fn part_to_nums(part: &str) -> HashSet<u32> {
    let mut nums: HashSet<u32> = HashSet::new();
    for n in part.trim().split(' ').filter(|s| !s.is_empty()) {
        nums.insert(n.trim().parse().unwrap());
    }
    nums
}

#[cfg(test)]
mod tests {
    use crate::load_input_trim_lines;

    const INPUT: &str = "Card 1: 41 48 83 86 17 | 83 86  6 31 17  9 48 53
    Card 2: 13 32 20 16 61 | 61 30 68 82 17 32 24 19
    Card 3:  1 21 53 59 44 | 69 82 63 72 16 21 14  1
    Card 4: 41 92 73 84 69 | 59 84 76 51 58  5 54 83
    Card 5: 87 83 26 28 32 | 88 30 70 12 93 22 82 36
    Card 6: 31 18 13 56 72 | 74 77 10 23 35 67 36 11";

    #[test]
    fn pt1() {
        // println!(">>> {:?}", crate::day4::lines_to_tickets(load_input_trim_lines(INPUT)));
        assert_eq!(
            crate::day4::process_games(crate::day4::lines_to_tickets(load_input_trim_lines(INPUT))),
            13
        );
    }

    #[test]
    fn pt2() {
        assert_eq!(
            crate::day4::process_games_pt2(crate::day4::lines_to_tickets(load_input_trim_lines(
                INPUT
            ))),
            30
        );
    }
}
