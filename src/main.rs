fn main() {
    env_logger::init();
    // println!("day1: {}, {}", aoc::day1::run_me(), aoc::day1::run_me_next());
    // println!(
    //     "day2: {}, {}",
    //     aoc::day2::run_me(),
    //     aoc::day2::run_me_next()
    // );
    // println!(
    //     "day3: {}, {}",
    //     aoc::day3::run_me(),
    //     aoc::day3::run_me_next()
    // );
    // println!(
    //     "day4: {}, {}",
    //     aoc::day4::run_me(),
    //     aoc::day4::run_me_next()
    // );
    println!(
        "day5: {}, {}",
        aoc::day5::run_me(),
        aoc::day5::run_me_next()
    );
}
