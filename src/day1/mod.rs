use once_cell::sync::Lazy;
use regex::Regex;
use std::collections::HashMap;
use std::fs;

static RE: Lazy<Regex> = Lazy::new(|| Regex::new(r"[a-zA-Z]").unwrap());
static RE2: Lazy<Regex> =
    Lazy::new(|| Regex::new(r"^(one|two|three|four|five|six|seven|eight|nine|[1-9])").unwrap());
static NUM_MAP: Lazy<HashMap<&str, u32>> = Lazy::new(|| {
    HashMap::from([
        ("one", 1),
        ("two", 2),
        ("three", 3),
        ("four", 4),
        ("five", 5),
        ("six", 6),
        ("seven", 7),
        ("eight", 8),
        ("nine", 9),
    ])
});

fn load_input() -> String {
    fs::read_to_string("src/day1/input").expect("Should have been able to read the file")
}

pub fn run_me_next() -> u32 {
    calibrate_next(&load_input())
    // calibrate_next(&"sixsevenfivefourxf4mzhmkztwonepzt")
}

fn parse_line_harder(line: &str) -> u32 {
    let mut cur = line.clone();
    let mut nums: Vec<u32> = vec![];
    while !cur.is_empty() {
        if let Some(m) = RE2.find(cur) {
            match m.as_str().parse() {
                Ok(n) => nums.push(n),
                Err(_) => nums.push(*NUM_MAP.get(m.as_str()).unwrap()),
            };
        };
        cur = &cur[1..];
    }
    // println!("{}    {}", line, nums.first().unwrap() * 10 + nums.last().unwrap());
    nums.first().unwrap() * 10 + nums.last().unwrap()
}

pub fn run_me() -> u32 {
    calibrate(&load_input())
}

fn parse_line(line: &str) -> u32 {
    let chrs: Vec<char> = RE
        .replace_all(line, "")
        .trim()
        .chars()
        .filter(|c| c.is_ascii_digit())
        .collect();
    let fst = chrs.first().unwrap().to_digit(10).unwrap();
    let lst = chrs.last().unwrap().to_digit(10).unwrap();
    fst * 10 + lst
}

pub fn calibrate(input: &str) -> u32 {
    input
        .split('\n')
        .filter(|l| !l.is_empty())
        .map(parse_line)
        .sum()
}
pub fn calibrate_next(input: &str) -> u32 {
    input
        .split('\n')
        .filter(|s| !s.is_empty())
        .map(|s| s.trim())
        .map(parse_line_harder)
        .sum()
}

#[cfg(test)]
mod tests {
    #[test]
    fn pt1() {
        let input = "1abc2
        pqr3stu8vwx
        a1b2c3d4e5f
        treb7uchet";
        assert_eq!(crate::day1::calibrate(input), 142);
    }

    #[test]
    fn pt2() {
        let input = "two1nine
        eightwothree
        abcone2threexyz
        xtwone3four
        4nineeightseven2
        zoneight234
        7pqrstsixteen";
        assert_eq!(crate::day1::calibrate_next(input), 281);
    }
}
