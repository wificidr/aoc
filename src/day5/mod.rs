use log;
use pest::Parser;
use pest_derive::Parser;
use std::collections::HashMap;
use std::fs;
use std::hash::Hash;

type Pair = (i64, i64);

#[derive(Parser)]
#[grammar = "src/day5/map.pest"]
struct MapParser;

#[derive(Debug)]
struct Map {
    seeds: Vec<i64>,
    nodes: HashMap<String, Vec<Vec<i64>>>,
}

pub fn run_me() -> i64 {
    let input = fs::read_to_string("src/day5/input").expect("could not read input file");
    min_location(&parse_input(&input))
}

pub fn run_me_next() -> i64 {
    let input = fs::read_to_string("src/day5/input").expect("could not read input file");
    min_location_pt2(&parse_input(&input))
}

fn parse_input(input: &str) -> Map {
    let file = MapParser::parse(Rule::file, input)
        .expect("unsuccessful parse") // unwrap the parse result
        .next()
        .unwrap();
    let mut map = Map {
        seeds: Vec::new(),
        nodes: HashMap::new(),
    };
    for line in file.into_inner() {
        match line.as_rule() {
            Rule::header => {
                for inner_rule in line.into_inner() {
                    match inner_rule.as_rule() {
                        Rule::key => (),
                        Rule::num_array => {
                            for num in inner_rule.into_inner() {
                                map.seeds.push(num.as_str().parse().unwrap());
                            }
                        }
                        x => {
                            unreachable!("{:?} is unreachable!", x)
                        }
                    }
                }
            }
            Rule::record => {
                let mut cur_key: &str = "";
                for inner_rule in line.into_inner() {
                    match inner_rule.as_rule() {
                        Rule::key => {
                            cur_key = inner_rule.as_str();
                            if map.nodes.get(cur_key).is_none() {
                                map.nodes
                                    .insert(cur_key.to_string(), vec![])
                                    .xor(Some(vec![]))
                                    .unwrap();
                            }
                        }
                        Rule::num_array => {
                            let mut num_vec: Vec<i64> = vec![];
                            for num in inner_rule.into_inner() {
                                num_vec.push(num.as_str().parse().unwrap());
                            }
                            map.nodes
                                .entry(cur_key.to_string())
                                .and_modify(|e| e.push(num_vec.clone()));
                        }
                        x => {
                            unreachable!("{:?} is unreachable!", x)
                        }
                    }
                }
            }
            Rule::EOI => (),
            _ => unreachable!(),
        }
    }
    map
}

fn min_location(map: &Map) -> i64 {
    *map.seeds
        .iter()
        .map(|s| walk_map(*s, map))
        .collect::<Vec<i64>>()
        .iter()
        .min()
        .unwrap()
}

fn walk_map(val: i64, map: &Map) -> i64 {
    let map_keys: Vec<&str> = vec![
        "seed-to-soil map:",
        "soil-to-fertilizer map:",
        "fertilizer-to-water map:",
        "water-to-light map:",
        "light-to-temperature map:",
        "temperature-to-humidity map:",
        "humidity-to-location map:",
    ];
    let mut cur_val = val;
    for key in map_keys {
        cur_val = lookup_map_val(key, cur_val, map);
    }
    cur_val
}

fn lookup_map_val(key: &str, val: i64, map: &Map) -> i64 {
    let mut fin = val;
    for nums in map.nodes.get(key).unwrap() {
        let (dst, src, off) = (nums[0], nums[1], nums[2]);
        match val {
            _ if val >= src && val <= src + off => {
                fin = dst + (val - src);
                break;
            }
            _ => (),
        };
    }
    fin
}

fn min_location_pt2(map: &Map) -> i64 {
    let mut seed: i64 = 0;
    let mut min_seed = i64::MAX;
    for (i, seed_n) in map.seeds.iter().enumerate() {
        if i % 2 == 0 {
            seed = *seed_n;
            log::debug!("seed {}\n\n", seed);
        } else {
            let cur_min = walk_map_pt2((seed, *seed_n), map);
            if cur_min < min_seed {
                min_seed = cur_min;
            }
        }
    }
    min_seed
}

fn walk_map_pt2(pair: Pair, map: &Map) -> i64 {
    let map_keys: Vec<&str> = vec![
        "seed-to-soil map:",
        "soil-to-fertilizer map:",
        "fertilizer-to-water map:",
        "water-to-light map:",
        "light-to-temperature map:",
        "temperature-to-humidity map:",
        "humidity-to-location map:",
    ];
    let mut partitions: Vec<Pair> = vec![pair];
    for key in map_keys {
        log::debug!("{}", key);
        log::debug!("old: {:?}", partitions);
        partitions = lookup_map_val_pt2(key, partitions, map);
        log::debug!("new: {:?}\n\n", partitions);
    }
    partitions.iter().map(|t| t.0).min().unwrap()
}

fn lookup_map_val_pt2(key: &str, in_parts: Vec<Pair>, map: &Map) -> Vec<Pair> {
    let mut partitions: Vec<Pair> = in_parts;
    let mut mapped_parts: Vec<Pair> = vec![];
    let mut new_parts: Vec<Pair>;
    for nums in map.nodes.get(key).unwrap() {
        new_parts = vec![];
        let (dst, src, off) = (nums[0], nums[1], nums[2]);
        for pair in partitions {
            let (orig, mapped) = partition_map(pair, (src, off), dst);
            for part in orig {
                new_parts.push(part);
            }
            for part in mapped {
                mapped_parts.push(part);
            }
        }
        partitions = new_parts;
        log::debug!("partitions: {:?}", partitions);
        log::debug!("mapped: {:?}", mapped_parts);
    }
    partitions.extend(mapped_parts);
    partitions.to_vec()
}

fn fst(pt: Pair) -> i64 {
    pt.0
}

fn lst(pt: Pair) -> i64 {
    pt.0 + pt.1 - 1
}

fn partition_map(given: Pair, seg: Pair, dst: i64) -> (Vec<Pair>, Vec<Pair>) {
    let shift = dst - seg.0;
    log::debug!(
        "given: {:?}, shift: {}, seg: {:?}, dst {}",
        given,
        shift,
        seg,
        dst
    );
    match (
        fst(given) - fst(seg),
        fst(given) - lst(seg),
        lst(given) - fst(seg),
        lst(given) - lst(seg),
    ) {
        // lst(given) == fst(seg), seg right of given
        (a, b, c, d) if a < 0 && b < 0 && c == 0 && d < 0 => {
            log::debug!("case 1, a: {}, b: {}, c: {}, d: {}", a, b, c, d);
            (vec![(fst(given), given.1 - 1)], vec![(fst(seg) + shift, 1)])
        }
        // seg overlaps right of given
        (a, b, c, d) if a < 0 && b < 0 && c > 0 && d < 0 => {
            log::debug!("case 2, a: {}, b: {}, c: {}, d: {}", a, b, c, d);
            (
                vec![(fst(given), fst(seg) - fst(given))],
                vec![(fst(seg) + shift, c + 1)],
            )
        }
        // lst(seg) == fst(given), seg left of given
        (a, b, c, d) if a > 0 && b == 0 && c > 0 && d > 0 => {
            log::debug!("case 3, a: {}, b: {}, c: {}, d: {}", a, b, c, d);
            (
                vec![(fst(given) + 1, given.1 - 1)],
                vec![(lst(seg) + shift, 1)],
            )
        }
        // given == seg
        (a, b, c, d) if a == 0 && b < 0 && c > 0 && d == 0 => {
            log::debug!("case 4, a: {}, b: {}, c: {}, d: {}", a, b, c, d);
            (vec![], vec![(dst, given.1)])
        }
        // seg overlaps left of given
        (a, b, c, d) if a > 0 && b < 0 && c > 0 && d > 0 => {
            log::debug!("case 5, a: {}, b: {}, c: {}, d: {}", a, b, c, d);
            (
                vec![(lst(seg) + 1, d)],
                vec![(fst(given) + shift, lst(seg) - fst(given) + 1)],
            )
        }
        // given completely overlaps seg
        (a, b, c, d) if a < 0 && b < 0 && c > 0 && d > 0 => {
            log::debug!("case 6, a: {}, b: {}, c: {}, d: {}", a, b, c, d);
            (
                vec![(fst(given), fst(seg) - fst(given)), (lst(seg) + 1, d)],
                vec![(fst(seg) + shift, lst(seg) - fst(seg) + 1)],
            )
        }
        // seg completely overlaps given
        (a, b, c, d) if a >= 0 && b < 0 && c > 0 && d <= 0 => {
            log::debug!("case 7, a: {}, b: {}, c: {}, d: {}", a, b, c, d);
            (vec![], vec![(fst(given) + shift, given.1)])
        }
        // seg lies outside given
        (a, b, c, d) => {
            log::debug!("fallthrough, a: {}, b: {}, c: {}, d: {}", a, b, c, d);
            (vec![given], vec![])
        }
    }
}

#[cfg(test)]
mod tests {
    use crate::day5::partition_map;
    fn init() {
        let _ = env_logger::builder().is_test(true).try_init();
    }

    const INPUT: &str = "seeds: 79 14 55 13

    seed-to-soil map:
    50 98 2
    52 50 48
    10 50 6
    
    soil-to-fertilizer map:
    0 15 37
    37 52 2
    39 0 15
    
    fertilizer-to-water map:
    49 53 8
    0 11 42
    42 0 7
    57 7 4
    
    water-to-light map:
    88 18 7
    18 25 70
    
    light-to-temperature map:
    45 77 23
    81 45 19 
    68 64 13
    
    temperature-to-humidity map:
    0 69 1
    1 0 69
    
    humidity-to-location map:
    60 56 37
    56 93 4";

    #[test]
    fn pt1() {
        let map = crate::day5::parse_input(INPUT);
        assert_eq!(
            crate::day5::lookup_map_val("seed-to-soil map:", 79, &map),
            81
        );
        assert_eq!(
            crate::day5::lookup_map_val("seed-to-soil map:", 14, &map),
            14
        );
        assert_eq!(
            crate::day5::lookup_map_val("seed-to-soil map:", 55, &map),
            57
        );
        assert_eq!(
            crate::day5::lookup_map_val("seed-to-soil map:", 13, &map),
            13
        );
        assert_eq!(crate::day5::walk_map(79, &map), 82);
        assert_eq!(crate::day5::walk_map(14, &map), 43);
        assert_eq!(crate::day5::walk_map(55, &map), 86);
        assert_eq!(crate::day5::walk_map(13, &map), 35);
        assert_eq!(crate::day5::min_location(&map), 35);
    }

    #[test]
    fn pt2() {
        init();
        // In the above example, the lowest location number can
        //  be obtained from seed number 82, which corresponds to
        //  soil 84, fertilizer 84, water 84, light 77, temperature 45,
        //  humidity 46, and location 46.
        //  So, the lowest location number is 46.
        assert_eq!(partition_map((5, 5), (5, 5), 5), (vec![], vec![(5, 5)]));
        assert_eq!(partition_map((5, 5), (10, 5), 5), (vec![(5, 5)], vec![]));
        assert_eq!(partition_map((5, 5), (0, 5), 5), (vec![(5, 5)], vec![]));
        assert_eq!(
            partition_map((5, 5), (1, 5), 5),
            (vec![(6, 4)], vec![(9, 1)])
        );
        assert_eq!(
            partition_map((5, 5), (9, 5), 9),
            (vec![(5, 4)], vec![(9, 1)])
        );
        assert_eq!(
            partition_map((5, 5), (2, 5), 5),
            (vec![(7, 3)], vec![(8, 2)])
        );
        assert_eq!(
            partition_map((5, 5), (8, 5), 8),
            (vec![(5, 3)], vec![(8, 2)])
        );
        assert_eq!(partition_map((5, 5), (2, 10), 5), (vec![], vec![(8, 5)]));
        assert_eq!(
            partition_map((2, 10), (5, 5), 5),
            (vec![(2, 3), (10, 2)], vec![(5, 5)])
        );
        assert_eq!(
            partition_map((79, 14), (50, 48), 52),
            (vec![], vec![(81, 14)])
        );
        let map = crate::day5::parse_input(INPUT);
        // assert_eq!(crate::day5::walk_map_pt2((79, 14), &map), 46);
        assert_eq!(crate::day5::min_location_pt2(&map), 46);
    }
}
