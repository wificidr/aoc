use std::cmp::Ordering;
use std::fs;

#[derive(Debug)]
struct Game {
    n: u32,
    bags: Vec<Bag>,
}

#[derive(Debug, Eq)]
struct Bag {
    red: u32,
    green: u32,
    blue: u32,
}

impl Ord for Bag {
    fn cmp(&self, other: &Self) -> Ordering {
        if self.red > other.red || self.green > other.green || self.blue > other.blue {
            Ordering::Greater
        } else if self == other {
            Ordering::Equal
        } else {
            Ordering::Less
        }
    }
}

impl PartialOrd for Bag {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl PartialEq for Bag {
    fn eq(&self, other: &Self) -> bool {
        self.red == other.red && self.green == other.green && self.blue == other.blue
    }
}

const BASE_BAG: Bag = Bag {
    red: 12,
    green: 13,
    blue: 14,
};

pub fn run_me() -> u32 {
    // Determine which games would have been possible if the bag had been loaded with only
    // 12 red cubes, 13 green cubes, and 14 blue cubes.
    // What is the sum of the IDs of those games?
    let input = fs::read_to_string("src/day2/input").expect("could not read input file");
    filter_possible(crate::load_input_trim_lines(&input))
}

pub fn run_me_next() -> u32 {
    let input = fs::read_to_string("src/day2/input").expect("could not read input file");
    find_mins(crate::load_input_trim_lines(&input))
}

fn parse_line(line: &str) -> Game {
    let parts: Vec<&str> = line.splitn(2, ':').collect();
    let game_num: u32 = parts[0].split(' ').collect::<Vec<&str>>()[1]
        .parse()
        .unwrap();
    let mut bags: Vec<Bag> = vec![];
    for round in parts[1].split(';') {
        let mut bag = Bag {
            red: 0,
            green: 0,
            blue: 0,
        };
        for cube in round.split(',') {
            let cube_pts: Vec<&str> = cube.trim().split(' ').collect();
            let n: u32 = cube_pts[0].parse().unwrap();
            match cube_pts[1] {
                "red" => bag.red = n,
                "green" => bag.green = n,
                "blue" => bag.blue = n,
                // who needs proper error handling?!
                _ => println!("could not parse line {}", line),
            }
        }
        bags.push(bag);
    }
    Game { n: game_num, bags }
}

fn filter_game(game: &Game) -> bool {
    for bag in &game.bags[..] {
        if bag > &BASE_BAG {
            return false;
        }
    }
    true
}

fn min_bag(bags: &Vec<Bag>) -> Bag {
    let mut min_red: u32 = 0;
    let mut min_green: u32 = 0;
    let mut min_blue: u32 = 0;
    for bag in bags {
        min_red = if bag.red > min_red { bag.red } else { min_red };
        min_green = if bag.green > min_green {
            bag.green
        } else {
            min_green
        };
        min_blue = if bag.blue > min_blue {
            bag.blue
        } else {
            min_blue
        };
    }
    Bag {
        red: min_red,
        green: min_green,
        blue: min_blue,
    }
}

fn find_mins(input: Vec<String>) -> u32 {
    input
        .iter()
        .map(|l| parse_line(l))
        .map(|g| min_bag(&g.bags))
        .map(|b| b.red * b.green * b.blue)
        .sum::<u32>()
}

fn filter_possible(input: Vec<String>) -> u32 {
    input
        .iter()
        .map(|l| parse_line(l))
        .filter(filter_game)
        .map(|g| g.n)
        .sum()
}

#[cfg(test)]
mod tests {
    use crate::load_input_trim_lines;

    #[test]
    fn pt1() {
        let input = "Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green
        Game 2: 1 blue, 2 green; 3 green, 4 blue, 1 red; 1 green, 1 blue
        Game 3: 8 green, 6 blue, 20 red; 5 blue, 4 red, 13 green; 5 green, 1 red
        Game 4: 1 green, 3 red, 6 blue; 3 green, 6 red; 3 green, 15 blue, 14 red
        Game 5: 6 red, 1 blue, 3 green; 2 blue, 1 red, 2 green";
        assert_eq!(
            crate::day2::filter_possible(load_input_trim_lines(input)),
            8
        );
    }

    #[test]
    fn pt2() {
        let input = "Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green
        Game 2: 1 blue, 2 green; 3 green, 4 blue, 1 red; 1 green, 1 blue
        Game 3: 8 green, 6 blue, 20 red; 5 blue, 4 red, 13 green; 5 green, 1 red
        Game 4: 1 green, 3 red, 6 blue; 3 green, 6 red; 3 green, 15 blue, 14 red
        Game 5: 6 red, 1 blue, 3 green; 2 blue, 1 red, 2 green";
        assert_eq!(crate::day2::find_mins(load_input_trim_lines(input)), 2286);
    }
}
